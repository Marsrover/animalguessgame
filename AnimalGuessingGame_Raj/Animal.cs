﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalGuessingGame_Raj
{
    [Serializable]
    public class Animal
    {
        public string Name { get; set; }
        public IList<string> Questions { get; set; }

        public Animal()
        {
            Questions = new List<string>();
        }
    }
}
