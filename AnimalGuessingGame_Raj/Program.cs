﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AnimalGuessingGame_Raj
{
    class Program
    {
        static AnimalList animalList;
        static readonly string fileName = "animal.binary";
        static void Main(string[] args)
        {
            Setup();
            Console.WriteLine("Starting the Animal Guessing Game!");
            Start();
            while (playAgain())
            {
                Start();
            }
        }
        private static bool playAgain()
        {
            Console.WriteLine("Do you want to play again?");
            var response = Console.ReadLine();
            if (response == "y")
                return true;
            else
                return false;
        }

        private static void AddNewAnimal()
        {
            var newAnimal = new Animal();
            Console.WriteLine("Enter the Animal");
            newAnimal.Name = Console.ReadLine();
            var inputString = string.Empty;
            Console.WriteLine("Enter a Question");
            while (inputString != "n")
            {
                inputString = Console.ReadLine();
                if (inputString != "n")
                {
                    newAnimal.Questions.Add(inputString);
                    Console.WriteLine("Next Question? if No further Questions then enter n");
                }
            }
            animalList.Animals.Add(newAnimal);

            SaveToFile();

        }

        private static void Setup()
        {
            if (!GetFromFile())
            {
                var elephant = new Animal();
                elephant.Name = "Elephant";
                elephant.Questions.Add("has a trunk ");
                elephant.Questions.Add("trumpets ");
                elephant.Questions.Add("is grey ");

                var lion = new Animal();
                lion.Name = "Lion";
                lion.Questions.Add("has a mane ");
                lion.Questions.Add("roars ");
                lion.Questions.Add("is yellow ");

                animalList = new AnimalList();
                animalList.Animals.Add(elephant);
                animalList.Animals.Add(lion);

                SaveToFile();
            }
        }

        private static void Start()
        {
            foreach (var animal in animalList.Animals)
            {
                Console.WriteLine(animal.Name);
            }

            Console.WriteLine("Choose any of the above Animal");
            Console.WriteLine("Ready?");
            Console.ReadLine();

            Console.WriteLine("Is it true about the animal ");

            var correctAnswer = true;
            foreach (var animal in animalList.Animals)
            {
                correctAnswer = true;
                foreach (var q in animal.Questions)
                {
                    Console.WriteLine(q);
                    if (Console.ReadLine() != "y")
                    {
                        correctAnswer = false;
                        break;
                    }
                }
                if (correctAnswer)
                {
                    Console.WriteLine("You had picked  " + animal.Name);
                    break;
                }
            }
            if (!correctAnswer)
            {
                Console.WriteLine("You win. I cannot guess");
            }

            Console.WriteLine("Do you want to add new Animal?");
            if (Console.ReadLine() == "y")
            {
                AddNewAnimal();
            }
        }

        private static void SaveToFile()
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream fs = new FileStream("animal.binary", FileMode.Create, FileAccess.Write, FileShare.None);
            using (fs)
            {
                bf.Serialize(fs, animalList);
            }
        }

        private static bool GetFromFile()
        {
            var fileExists = false;
            if (File.Exists(fileName))
            {
                fileExists = true;
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream("animal.binary", FileMode.Open, FileAccess.Read, FileShare.None);
                using (fs)
                {
                    animalList = (AnimalList)bf.Deserialize(fs);
                }
            }
            return fileExists;
        }

    }
}
