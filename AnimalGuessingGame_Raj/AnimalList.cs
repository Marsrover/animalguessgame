﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalGuessingGame_Raj
{
    [Serializable]
    public class AnimalList
    {
        public List<Animal> Animals { get; set; }
        public AnimalList()
        {
            Animals = new List<Animal>();
        }
    }
}
